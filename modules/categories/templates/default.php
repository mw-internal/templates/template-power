<?php

/*

type: layout

name: Default

description: List Navigation

*/

?>

<?php
$params['ul_class'] = '';
$params['ul_class_deep'] = '';

?>


<style>
    .module-categories .categories-list ul {
        list-style-type: none;
    }

    .module-categories .categories-list ul li {
        display: inline-block;
    }
</style>
<div class="isotope-product-filter col-xs-12">
    <?php category_tree($params); ?>
</div>

