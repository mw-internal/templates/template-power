<div class="col-xs-12 col-sm-6 pb-20">
    <label class="control-label">
        <?php _lang("First Name", "templates/power"); ?>
    </label>
    <input name="cc_first_name" type="text" value=""/>
</div>
<div class="col-xs-12 col-sm-6 pb-20">
    <label class="control-label">
        <?php _lang("Last Name", "templates/power"); ?>
    </label>
    <input name="cc_last_name" type="text" value=""/>
</div>
<div class="col-xs-12 col-sm-6 pb-20">
    <label class="control-label">
        <?php _lang("Credit Card", "templates/power"); ?>
    </label>
    <select name="cc_type">
        <option value="Visa" selected>
            <?php _lang("Visa", "templates/power"); ?>
        </option>
        <option value="MasterCard">
            <?php _lang("MasterCard", "templates/power"); ?>
        </option>
        <option value="Discover">
            <?php _lang("Discover", "templates/power"); ?>
        </option>
        <option value="Amex">
            <?php _lang("American Express", "templates/power"); ?>
        </option>
    </select>
</div>
<div class="col-xs-12 col-sm-6 pb-20">
    <label>
        <?php _lang("Credit Card Number", "templates/power"); ?>
    </label>
    <input name="cc_number" type="text" value=""/>
</div>
<div class="col-xs-12 pb-20">
    <label style="font-weight: bold;">
        <?php _lang("Expiration Date", "templates/power"); ?>
    </label>
    <div class="row m-t-0">
        <div class="col-xs-12 col-sm-6 ">
            <label><?php _lang("Month", "templates/power"); ?></label>
            <input name="cc_month" placeholder="" type="text" value=""/>
        </div>
        <div class="col-xs-12 col-sm-6 ">
            <label><?php _lang("Year", "templates/power"); ?></label>
            <input name="cc_year" placeholder="" type="text" value=""/>
        </div>
    </div>
</div>
<div class="col-xs-12 pb-20">
    <label>
        <?php _lang("Verification Code", "templates/power"); ?>
    </label>
    <input name="cc_verification_value" type="text" value=""/>
    <div class="cc_process_error"></div>
</div>