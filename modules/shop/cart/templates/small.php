<?php
/*
type: layout

name: Small

description: Small cart template



*/

?>
<?php if (is_array($data)) : ?>

    <?php
    $total_qty = 0;

    $total_price = 0;

    foreach ($data as $item) {

        $total_qty += $item['qty'];

        $total_price += $item['price'] * $item['qty'];

    }

    ?>


    <?php
    if (!isset($params['checkout-link-enabled'])) {
        $checkout_link_enanbled = get_option('data-checkout-link-enabled', $params['id']);
    } else {
        $checkout_link_enanbled = $params['checkout-link-enabled'];
    }
    ?>

    <?php if ($checkout_link_enanbled != 'n') : ?>
        <?php $checkout_page = get_option('data-checkout-page', $params['id']); ?>
        <?php if ($checkout_page != false and strtolower($checkout_page) != 'default' and intval($checkout_page) > 0) {
            $checkout_page_link = content_link($checkout_page) . '/view:checkout';
        } else {
            $checkout_page_link = checkout_url();
        }
        ?>

        <a class="cart-toggle mw-cart-small" href="<?php print $checkout_page_link; ?>" data-toggle="dropdown">
            <i class="pe-7s-cart"></i>
            <span class="bag"><?php print $total_qty; ?></span>
        </a>
    <?php endif; ?>
<?php else : ?>
    <a class="cart-toggle mw-cart-small" href="<?php print $checkout_page_link; ?>" data-toggle="dropdown">
        <i class="pe-7s-cart"></i>
        <span class="bag">0</span>
    </a>
<?php endif; ?>


<module type="shop/cart" template="dropdown" class="mini-cart-brief dropdown-menu text-left" id="inner_<?php print $params['id'] ?>"/>

