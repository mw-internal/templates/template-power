<?php

/*

type: layout

name: Checkout

description: Checkout cart template

*/

?>

    <h3 class="edit" rel="<?php print $params['id'] ?>" field="checkout_cart_title">
        <?php _lang('Your order', "templates/power"); ?>
    </h3>

<?php if (is_array($data)) : ?>
    <div class="order-table table-responsive mb-30">
        <table>
            <thead>
            <tr>
                <th class="product-name"><?php _lang("Product", "templates/power"); ?></th>
                <th class="product-total"><?php _lang("Total", "templates/power"); ?></th>
            </tr>
            </thead>
            <tbody>

            <?php
            $total = cart_sum();
            foreach ($data as $item) : ?>
                <tr class="mw-cart-item mw-cart-item-<?php print $item['id'] ?>">
                    <td class="product-name">
                        <?php print $item['title'] ?>
                        <?php if (isset($item['custom_fields'])): ?>
                            <?php print $item['custom_fields'] ?>
                        <?php endif ?>
                        <strong class="product-qty"> × <?php print $item['qty'] ?></strong>
                    </td>
                    <td class="product-total"><span class="amount"><?php print currency_format($item['price'] * $item['qty']); ?></span></td>
                </tr>
            <?php endforeach; ?>
            </tbody>

            <tfoot>
            <?php if (function_exists('cart_get_tax') and get_option('enable_taxes', 'shop') == 1) : ?>
                <tr>
                    <th><?php _lang("Tax", "templates/power"); ?></th>
                    <td><?php print currency_format(cart_get_tax()); ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <th><?php _lang("Total Price", "templates/power"); ?></th>
                <td>
                    <strong> <?php print currency_format($print_total); ?></strong>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
<?php else : ?>
    <h4 class="alert alert-warning">
        <?php _lang("Your cart is empty.", "templates/power"); ?>
    </h4>
<?php endif; ?>