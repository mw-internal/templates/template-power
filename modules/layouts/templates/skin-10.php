<?php

/*

type: layout

name: Contact block

position: 10

*/
?>

<div class="page-section section pt-60 pb-90 edit safe-mode nodrop" field="layout-skin-4-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <h1 class="edit mb-40">Contact us</h1>

        <div class="row mb-40">
            <!-- Contact Map -->
            <div class="contact-map col-md-6 col-xs-12 mb-40">
                <module type="google_maps"/>
            </div>
            <!-- Contact Form -->
            <div class="contact-form col-md-6 col-xs-12 mb-40">
                <h3 class="edit" field="form_title" rel="content">Send Your Massage</h3>

                <module type="contact_form" template="default"/>
            </div>
        </div>
        <!-- Contact Info -->
        <div class="row">
            <div class="edit" field="content" rel="content">
                <div class="contact-info col-md-4 text-center mb-30">
                    <a href="tel:+88587485698765">Phone: +88 77 666 555 44</a>
                </div>
                <div class="contact-info col-md-4 text-center mb-30">
                    <a href="mailto:outside@email.com">Email: outside@email.com</a>
                </div>
                <div class="contact-info col-md-4 text-center mb-30">
                    <p>Address: house 0922,Road 3,Dhaka</p>
                </div>
            </div>
        </div>
    </div>
</div>