<?php

/*

type: layout

name: Login page

description: Login page

*/

?>
<?php $user = user_id(); ?>
<?php $have_social_login = false; ?>
<script>mw.moduleCSS("<?php print modules_url(); ?>users/login/templates.css")</script>

<div id="mw-login" class="checkout-form">
    <?php if ($user != false): ?>
        <div>
            <module type="users/profile"/>
        </div>
    <?php else: ?>
        <div class="col-xs-12 pb-10">
            <h2><?php _lang("Login", "templates/power"); ?></h2>
        </div>
        <div id="user_login_holder_<?php print $params['id'] ?>">
            <div class="col-xs-12">
                <form method="post" id="user_login_<?php print $params['id'] ?>" class="clearfix" action="#">
                    <div class="row pb-20">
                        <div class="col-xs-12 pb-20">
                            <label><?php _lang("Email or username", "templates/power"); ?></label>
                            <input name="username" type="text"/>
                        </div>

                        <div class="col-xs-12">
                            <label><?php _lang("Password", "templates/power"); ?></label>
                            <input name="password" type="password"/>
                        </div>
                    </div>

                    <?php if (isset($login_captcha_enabled) and $login_captcha_enabled): ?>
                        <module type="captcha" />
                    <?php endif; ?>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="alert" style="margin: 0;display: none;"></div>

                        <div class="col-xs-12 col-sm-6">
                            <a class="" href="<?php print forgot_password_url(); ?>"><?php _lang("Forgot password", "templates/power"); ?> ?</a>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <input class="btn btn-default pull-right" type="submit" value="<?php _lang("Login", "templates/power"); ?>"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endif; ?>
</div>
